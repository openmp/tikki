#include <omp-tools.h>
#include "kaapi_trace.h"

/* Return human readable name from encoding of the psource information */
/* psource: Input format: ";../../plasma/src/timing.inc;run;129;15;;"
   name == task name, should replace the function name
   Output format: "func: <funcname>\nfile: <filename>\nline: <linenumber>"
*/
static char* libomp_filter_func(char* output, int size, const char* psource, const char* name);

#define KAAPI_MAX_NESTEDPARALLEL_TEAM 8

/* ------------ basic type to store information */
typedef struct {
  kaapi_descrformat_t* fdescr;                    /* of the top stack ! */
  void*                task;                      /* task on the top stack ! */
  kaapi_perf_counter_t accum[KAAPI_PERF_ID_MAX];  /* each of size eventset at most 64 */
} kaapi_taskstackentry_t;

typedef struct {
  int top;
  int capacity;
  kaapi_taskstackentry_t* stack;
  kaapi_perf_counter_t t0_task[KAAPI_PERF_ID_MAX];/* starting values of perfctr for the current task */
  kaapi_perf_counter_t t0[KAAPI_PERF_ID_MAX];     /* starting values of perfctr for the thread */
} kaapi_stack2ompt_t;

typedef struct {
  int top;
  kaapi_tracelib_team_t* stack[KAAPI_MAX_NESTEDPARALLEL_TEAM];/* starting values of perfctr for the current task */
} kaapi_stack2team_t;

/* In this version assume to ne recopy or extend array of such data type entry */
typedef struct {
  kaapi_tracelib_thread_t*    kproc;
  kaapi_stack2ompt_t          pstack;
  kaapi_stack2team_t          tstack;
} kaapi_ompt_thread_info_t __attribute__((aligned(64)));


#if 0
static kmp_lock_t                __kaapi_global_lock;
#endif
extern kaapi_ompt_thread_info_t* __kaapi_oth_info;
extern size_t                    __kaapi_oth_info_capacity;
extern ompt_get_thread_data_t    ompt_get_thread_data;
extern ompt_get_unique_id_t    ompt_get_unique_id;

