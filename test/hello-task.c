#include <stdio.h>
#include <unistd.h>
#include <omp.h>
#include <math.h>


int array[] = { 1, 2, 3, 4};

/* Extern function: required binary to be link against libtikki.so
*/
extern void ompt_set_task_name(const char* name );


int main()
{
for (int i=0; i<3; ++i)
{
#pragma omp parallel
#pragma omp master
  {
    printf("Hello from %i\n", omp_get_thread_num());
    ompt_set_task_name("hello");

    #pragma omp task
    {
      sleep(2);
      printf("Hey there\n");

      ompt_set_task_name("sub hello");
      #pragma omp task 
      {
        sleep(2);
        printf("Sub Hey there\n");
      }
     
      ompt_set_task_name("sub hello");
      #pragma omp task 
      {
        sleep(2);
        printf("Sub Hey there\n");

        ompt_set_task_name("sub sub hello");
        #pragma omp task 
        {
          sleep(2);
          printf("Sub Sub Hey there\n");
        }
        ompt_set_task_name("sub sub hello");
        #pragma omp task 
        {
          sleep(2);
          printf("Sub Sub Hey there\n");
        }
      }
    }
    printf("Task wait begin\n");
    #pragma omp taskwait
    printf("Task wait end\n");

    ompt_set_task_name("Parent of other tasks");
    #pragma omp task 
    for (int i = 0; i < 4; i++) {
      ompt_set_task_name("other tasks");
      #pragma omp task depend(in: array[i]) depend(inout: array[(i+1)%4])
      {
        double d;
        array[(i+1)%4] = array[i];
        for (int j=0; j<100000; ++j) 
          d += sin(M_PI/j)*cos(M_PI/i);
	sleep(2);
        printf("Hey %i\n", i);
      }
    }
  }
sleep(1);
}
  printf("array: ");
  for (int i = 0; i < 4; i++)
    printf("%i, ", array[i]);
  printf("\n");
  return 0;
}
