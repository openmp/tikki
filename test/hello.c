#include <stdio.h>
#include <omp.h>
#include <unistd.h>


int main()
{
printf("Before parallel region 1,  %i\n", omp_get_thread_num());
#pragma omp parallel
  {
    printf("Hello from %i\n", omp_get_thread_num());
    #pragma omp barrier
    #pragma omp master
    {
      #pragma omp task
      printf("%i:: Master thread\n", omp_get_thread_num());
      
      #pragma omp taskwait
    }
  }
printf("After parallel region 1, %i\n\n\n\n", omp_get_thread_num());
sleep(10);
printf("Before parallel region 2,  %i\n", omp_get_thread_num());
#pragma omp parallel
  {
    printf("Parallel 2, hello from %i\n", omp_get_thread_num());
  }
printf("After parallel region 2, %i\n\n\n\n", omp_get_thread_num());
  return 0;
}
