#include <iostream>
#include <string>
#include <omp.h>


int array[] = { 1, 2, 3, 4};

/* Extern function: required binary to be link against libtikki.so
*/
extern "C" void ompt_set_task_name(const char* name );

class task_gen {
public:
  task_gen( const std::string& name, int n )
   : _name(name), _count(n) {}
  
  void submit()
  {
    for (int i=0; i<_count; ++i)
    {
      ompt_set_task_name(_name.c_str());
      #pragma omp task
      { std::cout << "In task " << i << "/" << _name << std::endl;
      }
    }
  }
protected:
  std::string _name;
  int _count;
};
      
  

int main()
{
  task_gen tg1("sub TaskGen1", 3);
  task_gen tg2("sub TaskGen2", 3);
#pragma omp parallel
#pragma omp master
  {
    ompt_set_task_name("TaskGen1");
    #pragma omp task
    {
      tg1.submit();
    }

    ompt_set_task_name("TaskGen2");
    #pragma omp task
    {
      tg2.submit();
    }
  }
  return 0;
}
