
#include <cstdio>
#include <cstdlib>
#include <sys/types.h>
#include <unistd.h>
#include <omp-tools.h>
#include "kaapi_trace_lib.h"

#define CALLBACK(name, ...)\
  void name##_action(__VA_ARGS__);
#include "ompt_callbacks.def"


kaapi_ompt_thread_info_t* __kaapi_oth_info = 0;
size_t                    __kaapi_oth_info_capacity = 256;
ompt_get_thread_data_t ompt_get_thread_data;
ompt_get_unique_id_t ompt_get_unique_id;
#include <atomic>
std::atomic<uint64_t> myuid(0);



extern "C" {

/* Internal function
*/
extern void tikki_ompt_set_task_name(const char* name );
extern void tikki_ompt_set_task_data(int count, void** data, size_t* size, int* mode );

extern void ompt_set_task_name(const char* name );// __attribute__((weak));

void __tikki_ompt_set_task_name(const char* name )
{ tikki_ompt_set_task_name(name); }
void __tikki_ompt_set_task_data(int count, void** data, size_t* size, int* mode )
{ tikki_ompt_set_task_data(count, data, size, mode); }

/* Exported function as extension to be called by application
*/
void ompt_set_task_name(const char* name ) //__attribute__((weak))
{ 
  tikki_ompt_set_task_name(name);
}

//__attribute__ ((weak, alias ("__tikki_ompt_set_task_name")));
extern void ompt_set_task_data(int count, void** data, size_t* size, int* mode ) 
{
  tikki_ompt_set_task_data(count, data, size, mode);
}
//__attribute__ ((weak, alias ("__tikki_ompt_set_task_data")));
} // extern "Cc



void initTool(ompt_function_lookup_t lookup) 
{
  int err = 0;
  __kaapi_oth_info = (kaapi_ompt_thread_info_t*)calloc(__kaapi_oth_info_capacity, sizeof(kaapi_ompt_thread_info_t));
  err = kaapi_tracelib_init( getpid() );
  if (err !=0)
    printf("[OMP-TRACE] TiKKi tracing, init error:%i, version: %s\n", err, get_kaapi_version());
  else
    printf("[OMP-TRACE] TiKKi tracing version: '%s'\n",get_kaapi_version());
  //printf("oth_info %p\n", __kaapi_oth_info);
  ompt_set_callback_t ompt_set_callback = (ompt_set_callback_t) lookup("ompt_set_callback");
  ompt_get_thread_data = (ompt_get_thread_data_t) lookup("ompt_get_thread_data");
  ompt_get_unique_id = (ompt_get_unique_id_t) lookup("ompt_get_unique_id");
  // FIXME: restore the call to omp get unique id
  ompt_get_unique_id = []() {
    return ++myuid;
  };

  //ompt_set_task_name = tikki_ompt_set_task_name;
  //ompt_set_task_data = tikki_ompt_set_task_data;

#define CALLBACK(name, ...)                       \
  do{                                                           \
    if (ompt_set_callback(name, (ompt_callback_t)name##_action) ==   \
        ompt_set_never)                                         \
    fprintf(stderr, "0: Could not register callback '" #name "'\n");   \
  }while(0);

#include "ompt_callbacks.def"
}

extern "C" {
  int ompt_initialize(
      ompt_function_lookup_t lookup,
      int initial_device_num,
      ompt_data_t* tool_data)
  {
    initTool(lookup);
    return 1; //success
  }

  void ompt_finalize(ompt_data_t* data)
  {
    kaapi_tracelib_fini();
    free(__kaapi_oth_info);
    fprintf(stderr, "[OMP-TRACE] Exiting Tikki tool\n");
  }

  ompt_start_tool_result_t* ompt_start_tool(
      unsigned int omp_version,
      const char *runtime_version)
  {
    fprintf(stderr, "[OMP-TRACE] Loading TiKKi tool\n");
    static ompt_start_tool_result_t ompt_start_tool_result = {&ompt_initialize,&ompt_finalize,0};
    return &ompt_start_tool_result;
  }
}

