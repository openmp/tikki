/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include <ctype.h>
#include <errno.h>
#include <stdarg.h>
#include <limits.h>
#include <omp.h>
#include <omp-tools.h>
#include <iostream>
#include <atomic>

#if LIBOMP_USE_NUMA
#ifndef _GNU_SOURCE
#    define _GNU_SOURCE
#endif
#include <sched.h> /* sched_getcpu */
#endif

/* used to only print event */
#define USE_KAAPI 1

#include "kaapi_trace_lib.h"
#include "kaapi_error.h"

#define LOG 0


std::atomic<uint64_t> unique_thread_id(1);
std::atomic<uint64_t> unique_parallel_id(1);

#define MAX_PARAM 16
typedef struct { 
  int     count;
  void*   data[MAX_PARAM];
  size_t  size[MAX_PARAM];
  int     mode[MAX_PARAM];
} task_data_info_t;


typedef struct tikki_task_id_s {
  uint64_t          id;
  uint64_t          pid;
  const void        *task_ptr;
  int               isexplicit;
  kaapi_descrformat_t* fdescr;
  ompt_dependence_t *deps;
  int               ndeps;
  const char*       name;
  task_data_info_t  datainfo;
} tikki_task_id_t;


/* Return human readable name from encoding of the psource information */
/* Input format: ";../../plasma/src/timing.inc;run;129;15;;" 
   Output format: "func: <funcname>\nfile: <filename>\nline: <linenumber>"
*/
char* libomp_filter_func(char* output, int size, const char* psource, const char* name)
{
  char* buffer = output;
  const char* p0;
  const char* p1;
  --size;
  if (psource ==0) goto return_fast;
  if (size <=0) return 0;

  /* pathname starting position */
  p0 = psource+1;

  /* functionname start: else name is the function name */
  p1 = strstr( p0+1, ";");
  if (p1 ==0)
  {
return_fast:
    if (name)
      return strcpy( output, name );
    else
      return 0;
  }

  /* find the filename in the path */
  p0 = p1-1;
  while (p0 != psource)
  {
    --p0;
    if (*p0== '/') break;
    if (*p0== ';') break;
  }
  ++p0;

  /* recopy file: <function name > */
  if ((buffer+5 - output) >= size) return output;
  ++p1;
  strcpy(buffer,"func: ");
  buffer += 6;
  if (name)
  {
    while (*name != 0)
    {
      if ((buffer - output) >= size) return output;
      *buffer++ = *name++;
    }
  }
  for (; *p1 != ';'; )
  {
    if ((buffer - output) >= size) return output;
    if (name)
      p1++;
    else
      *buffer++ = *p1++;
  }
  *buffer++ = '\n';
  ++p1;

  /* recopy the filename */
  if ((buffer+5 - output) >= size) return output;
  strcpy(buffer,"file: ");
  buffer += 6;
  for (; *p0 != ';'; )
  {
    if ((buffer - output) >= size) return output;
    *buffer++ = *p0++;
  }
  *buffer++ = '\n';

  /* recopy line number */
  if ((buffer+5 - output) >= size) return output;
  strcpy(buffer,"line: ");
  buffer += 6;
  for (; *p1 != ';'; )
  {
    if ((buffer - output) >= size) return output;
    *buffer++ = *p1++;
  }
  if ((buffer - output) >= size) return output;
  *buffer = 0;

  return output;
}


/*
*/
static void ompt_mode_decoder( ompt_dependence_type_t odt, int* mode )
{
  *mode = KAAPI_ACCESS_MODE_VOID;
  switch (odt)
  {
    case ompt_dependence_type_in:
    {
      *mode =KAAPI_ACCESS_MODE_R;
      break;
    }
    case ompt_dependence_type_out:
    {
      *mode =KAAPI_ACCESS_MODE_W;
      break;
    }
    case ompt_dependence_type_inout:
    {
      *mode =KAAPI_ACCESS_MODE_R|KAAPI_ACCESS_MODE_W;
      break;
    }
    case ompt_dependence_type_mutexinoutset:
    {
      *mode =KAAPI_ACCESS_MODE_C|KAAPI_ACCESS_MODE_CW;
      break;
    }
    default:
    {
      static int alreadydisplay = 0;
      if (alreadydisplay ==0)
      {
        printf("*** OpenMP dependence type: %i not implemented\n", odt );
        alreadydisplay = 1;
      }
    }
  }
}


/*
*/
static void ompt_decoder( ompt_dependence_t* dep, int i, void** addr, size_t* len, int* mode /*, size_t* len */ )
{
  *addr = dep[i].variable.ptr;
  //*len  = dep[i].variable_len;
  *len = 1;
  *mode = KAAPI_ACCESS_MODE_VOID;
  if (dep[i].dependence_type & ompt_dependence_type_in)
    *mode |=KAAPI_ACCESS_MODE_R;
  if (dep[i].dependence_type & ompt_dependence_type_out)
    *mode |=KAAPI_ACCESS_MODE_W;
  if (dep[i].dependence_type & ompt_dependence_type_inout)
    *mode =KAAPI_ACCESS_MODE_R|KAAPI_ACCESS_MODE_W;
  //if (dep[i].dependence_flags.commute)
    //*mode |=KAAPI_ACCESS_MODE_CW;
  //ompt_mode_decoder(dep[i].dependence_type, mode );
}


/*
*/
static inline void realloc_ifrequired( size_t size )
{
#if 0
  kmp_uint32 gtid = __kmp_entry_gtid();
  __kmp_acquire_lock(&__kaapi_global_lock, gtid);
  if (__kaapi_oth_info_capacity <size)
  {
    size_t old_size = __kaapi_oth_info_capacity;
    if (__kaapi_oth_info_capacity == 0) __kaapi_oth_info_capacity = 1;
    while (__kaapi_oth_info_capacity <size)
      __kaapi_oth_info_capacity *= 2;

    kaapi_ompt_thread_info_t* new_oth_info
        = (kaapi_ompt_thread_info_t*)realloc(__kaapi_oth_info, __kaapi_oth_info_capacity*sizeof(kaapi_ompt_thread_info_t) );
    for (int i=0; i< old_size; ++i)
      new_oth_info[i].stack = __kaapi_oth_info[i].stack;
    memset(&new_oth_info[old_size], 0, __kaapi_oth_info_capacity - old_size);
    __kaapi_oth_info = new_oth_info;
  }
  __kmp_release_lock(&__kaapi_global_lock, gtid);
#else
  exit(0);
#endif
}

using namespace std;

void ompt_callback_thread_begin_action(
    ompt_thread_t thread_type,
    ompt_data_t *thread_data
    )
{
#if 0
  int thread_id = thread_data->value = ompt_get_unique_id();
#else
  // NOTE: while it may be interesting to use ompt_get_unique_id, this number
  // may grow a lot (whereas __kaapi_oth_info has a limited capacity).
  // Using the omp_get_thread_num may work, but we can also simply use a
  // custom unique_thread_id.
  int thread_id = thread_data->value = unique_thread_id++;
  //int thread_id = thread_data->value = omp_get_thread_num();
#endif

#if LOG
  printf("%" PRIu64 ": ompt_event_thread_begin: thread_id=%" PRIu64 "\n", thread_data->value, (uint64_t)thread_type);
#endif
  kaapi_assert ( thread_id < __kaapi_oth_info_capacity );
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
#if LIBOMP_USE_NUMA
  int cpu = sched_getcpu();
#endif
  koti->kproc = kaapi_tracelib_thread_init(
    thread_id-1,
#if LIBOMP_USE_NUMA
    cpu,
    __kmp_cpu2node(cpu), /* ok because NUMA == linux. Note that information is incorrect; LibOMP move thread
at the begining of the parallel region */
#else
    0, 0,
#endif
    0
  );
  koti->pstack.top = 0;
  koti->pstack.capacity = 256;
  koti->pstack.stack = (kaapi_taskstackentry_t*)malloc(
    sizeof(kaapi_taskstackentry_t) * koti->pstack.capacity /*  */
  );
  koti->tstack.top = 0;
  kaapi_tracelib_thread_start( koti->kproc );
}

void ompt_callback_thread_end_action(
    ompt_data_t *thread_data
    )
{
#if LOG
  printf("%" PRIu64 ": ompt_event_thread_end: thread_id=%" PRIu64 "\n", thread_data->value, thread_data->value);
#endif
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_data->value];
  kaapi_tracelib_thread_stop( koti->kproc );
  kaapi_tracelib_thread_fini( koti->kproc );
}

void ompt_callback_parallel_begin_action (
  ompt_data_t *encountering_task_data,
  const ompt_frame_t *encountering_task_frame,
  ompt_data_t *parallel_data,
  unsigned int requested_parallelism,
  int flags,
  const void *codeptr_ra
)
{
  ompt_data_t *thread_data = ompt_get_thread_data();
  //parallel_data->value = ompt_get_unique_id();
  parallel_data->value = unique_parallel_id++;
  uint64_t thread_id = thread_data->value;
  tikki_task_id_t *task = (tikki_task_id_t *)encountering_task_data->ptr;

#if LOG
  printf("%" PRIu64 ": omp threadid:%" PRIu64 ": ompt_event_parallel_begin: parent_task_frame=%p, task_id=%" PRIu64 ", requested_team_size=%" PRIu32 ", parallel_function=%p, parallel_data: %" PRIu64 "\n", thread_id, (uint64_t)omp_get_thread_num(),
      encountering_task_frame,
      task->id,
      requested_parallelism, codeptr_ra,
      parallel_data->value
      );
#endif

  /* TODO here: the key is the way several instances of the same parallel region are collapsed
     to compute statistics. 
     Default is to take key equal to the parallel_function pointer.

     Each tracelib_team_t is a per thread data structure used to capture performance counter
     for a parallel region.
     The set of kaapi_tracelib_team_t that forms the OMP parallel region is computed at the
     terminaison of the library in order to report overhead at the end, after the computation.
     The relation ship between a team and its parent team is known only on the master team thread.
     Statistics are computed among the different instances at the terminaison of the library.
     */
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  void* key = const_cast<void *>(codeptr_ra);
#if LIBOMP_USE_NUMA
  /* correct numaid here because when thread_begin, affinity seems not good with libomp */
  koti->kproc->numaid = __kmp_cpu2node(sched_getcpu());
#endif
  kaapi_tracelib_team_t* parent_team = 0;
  if (koti->tstack.top !=0)
    parent_team = koti->tstack.stack[koti->tstack.top-1];
  kaapi_tracelib_team_t* team = kaapi_tracelib_team_init(
      koti->kproc,
      key,
      parent_team,
      codeptr_ra,
      0,
      0, /* an other possible name in place for the defaut name encoded in psource */
      libomp_filter_func
      );
  koti->tstack.stack[koti->tstack.top] = team;
  kaapi_tracelib_team_start(koti->kproc,
      team,
      koti->tstack.top >0 ?  koti->tstack.stack[koti->tstack.top-1]: 0,
      parallel_data->value
      );
  ++koti->tstack.top;
}

void
ompt_callback_parallel_end_action(
  ompt_data_t *parallel_data,
  ompt_data_t *encountering_task_data,
  int flags,
  const void *codeptr_ra)
{
  uint64_t thread_id = ompt_get_thread_data()->value;
  /* end implicit task here */
  tikki_task_id_t *task = (tikki_task_id_t *)encountering_task_data->ptr;
#if LOG
  printf("%" PRIu64 ": on_ompt_event_parallel_end: parallel_id=%" PRIu64 ", task_id=%" PRIu64 "\n", thread_id, parallel_data->value, task->id);
#endif
  /* end of the parallel region */
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
  int idxtop = --koti->tstack.top;
  kaapi_tracelib_team_stop( koti->kproc,
      koti->tstack.stack[idxtop],
      idxtop >0 ?  koti->tstack.stack[idxtop-1]: 0,
      parallel_data->value
  );
  kaapi_tracelib_team_fini( koti->kproc, koti->tstack.stack[idxtop] );
}


/*
*/
__thread const char* next_name = 0;
extern "C" 
void tikki_ompt_set_task_name(const char* name )
{
  next_name = name;
}


/*
*/
__thread task_data_info_t next_data_info = { 0 };
  
extern "C" 
void tikki_ompt_set_task_data(int count, void** data, size_t* size, int* mode ) 
{
#if LOG
printf("In %s: count: %i, data[0]:%p, size[0]: %li, mode[0]: %i\n", __func__, 
    count, data[0], size[0], mode[0] );
#endif
  next_data_info.count = count;
  memcpy( &next_data_info.data, data, sizeof(void*)*count );
  memcpy( &next_data_info.size, size, sizeof(size_t)*count );
  memcpy( &next_data_info.mode, mode, sizeof(int)*count );
}

void
ompt_callback_task_create_action(
    ompt_data_t *parent_task_data,     /* id of parent task            */
    const ompt_frame_t *parent_frame,  /* frame data for parent task   */
    ompt_data_t* new_task_data,        /* id of created task           */
    int type,
    int has_dependences,
    const void *codeptr_ra)            /* pointer to outlined function */
{
  uint64_t thread_id = ompt_get_thread_data()->value;
  tikki_task_id_t *task = (tikki_task_id_t *)malloc(sizeof(tikki_task_id_t));
  tikki_task_id_t *parent_task = (tikki_task_id_t*)parent_task_data->ptr;
  new_task_data->ptr = task;
  task->id = ompt_get_unique_id();
  task->pid = parent_task->id;
  task->task_ptr = codeptr_ra;
  task->isexplicit = 1;
  task->ndeps = 0;
  task->name = next_name;
  task->datainfo = next_data_info;
  /* */
  task->fdescr = kaapi_tracelib_register_fmtdescr(
      0,
      // TODO: get codeptr_ra there
      (void *)task->task_ptr,
      0, //loc
      task->name,
      libomp_filter_func
  );

#if LOG
  if (parent_task_data) {
    printf("%" PRIu64 ": ompt_task_create: parent_id=%" PRIu64 ", task_id=%" PRIu64\
           ", type=%i, has_dep=%i, ptr=%p, name=%s\n", thread_id,
           parent_task->id, task->id, type,
           has_dependences, task->task_ptr, next_name ==0 ? "<no name>" : next_name);
  } else {
    // NOTE: while not occurring with the current libOMP's master, I've seen
    // this happening in previous commits.
    printf("%" PRIu64 ": ompt_task_create: parent_id=<null>, task_id=%" PRIu64\
           ", type=%i, has_dep=%i, ptr=%" PRIu64 "\n", thread_id,
           task->id, type,
           has_dependences, (uint64_t)task->task_ptr);
  }
#endif
  next_name = 0;
}


void
ompt_callback_task_schedule_action(
    ompt_data_t *prior_task_data,
    ompt_task_status_t prior_task_status,
    ompt_data_t *next_task_data)
{
  uint64_t thread_id = ompt_get_thread_data()->value;
  tikki_task_id_t *prior_task =0;
  if (prior_task_data->ptr) prior_task = (tikki_task_id_t *)prior_task_data->ptr;
  tikki_task_id_t *next_task = 0;
  if (next_task_data->ptr) next_task = (tikki_task_id_t *)next_task_data->ptr;
#if LOG
  printf("%" PRIu64 ": ompt_callback_task_schedule: prior_task=%" PRIu64 ", status=%i, next_task: %" PRIu64 "\n", thread_id, prior_task->id, prior_task_status, next_task->id);
#endif
  if (prior_task_data->ptr) {
    // We are ending a task
    /* thread 0 ends the parallel region
       But because the end of implicit task is not related to
       the end of the parallel region, we define here the end of implicit task.
       */
    kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
    int idxtop = --koti->pstack.top;

    /* reset that accumulated into */
    memset( koti->pstack.stack[idxtop].accum, 0, sizeof(koti->pstack.stack[idxtop].accum));
    kaapi_tracelib_task_end(
        koti->kproc,
        koti->pstack.stack[idxtop].task,
        0,
        0,
        koti->pstack.stack[idxtop].fdescr,
        koti->pstack.t0,
        koti->pstack.stack[idxtop].accum
        );
    kaapi_tracelib_thread_switchstate(koti->kproc);
#if LOG
    printf("%i::POP task: thread_id: %i, task: %p, top:%i\n", __LINE__, (int)thread_id, koti->pstack.stack[idxtop].task, idxtop);
#endif
    //free(prior_task);
  }
  //((prior_task_status != ompt_task_complete) (((prior_task_status == ompt_task_yield)|| (prior_task_status == ompt_task_switch))
  if (next_task_data->ptr !=0)
  {
    // We are starting a task
#if LOG
    printf("%" PRIu64 ": ompt_callback_task_schedule:: next task_begins: task_id=%" PRIu64 ", status=%i, deps: %p, codeptr_ra: %p\n", thread_id, next_task->id, prior_task_status, next_task->deps, next_task->task_ptr);
#endif
    /* This is code for explicit task begin.
    */
    kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
    kaapi_tracelib_thread_switchstate(koti->kproc);
    
    int idxtop = koti->pstack.top;
    koti->pstack.stack[idxtop].fdescr = next_task->fdescr;
    koti->pstack.stack[idxtop].task   = (void*)next_task->id;

#if LOG
    printf("%i::PUSH task: thread_id: %i, task: %p, top:%i\n", __LINE__, (int)thread_id, koti->pstack.stack[idxtop].task, idxtop);
#endif

    kaapi_tracelib_task_begin(
        koti->kproc,
        (kaapi_task_id_t)next_task->id,
        (kaapi_task_id_t)next_task->pid,
        next_task->fdescr->fmtid,
        next_task->isexplicit,
        0, 0, 0,
        0, 0,
        koti->pstack.t0,
        koti->pstack.top ==0 ? 0 : koti->pstack.stack[idxtop-1].accum
        );
    ++koti->pstack.top;
    
    if (next_task->ndeps > 0) {
      kaapi_tracelib_task_access(
          koti->kproc,
          (kaapi_task_id_t)next_task->id,
          next_task->ndeps,
          (void*)next_task->deps,
          0,
          (void*)0,
          (void (*)(void*, int, void**, size_t*, int*))ompt_decoder
      );
    }
    if (next_task->datainfo.count)
    {
      kaapi_tracelib_task_data(
          koti->kproc,
          (kaapi_task_id_t)next_task->id,
          next_task->datainfo.count,
          next_task->datainfo.data,
          next_task->datainfo.size,
          next_task->datainfo.mode,
          (void (*)(int, int*))ompt_mode_decoder
      );
      next_task->datainfo.count = 0;
    }
  }
}

void
ompt_callback_implicit_task_action (
    ompt_scope_endpoint_t endpoint,
    ompt_data_t *parallel_data,
    ompt_data_t *task_data,
    unsigned int actual_parallelism,
    unsigned int index,
    int flags
    )
{
  uint64_t thread_id = ompt_get_thread_data()->value;
  if (endpoint == ompt_scope_begin)
  {
    tikki_task_id_t *task = (tikki_task_id_t *)malloc(sizeof(tikki_task_id_t));
    task->id = ompt_get_unique_id();
    task->pid = -1;
    task->task_ptr = 0;
    task->isexplicit = 0;
    task_data->ptr = task;
    task->ndeps = 0;
    task->name = "<implicit>";
#if LOG
    printf("%" PRIu64 ": ompt_event_implicit_task_action: begin. parallel_id=%" PRIu64 ", task_id=%" PRIu64 ", @task data=%p\n", thread_id, parallel_data->value, task->id, task_data);
#endif
    char buff[30];
    snprintf(buff, 30, "<implicit>") ;
    kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
    task->fdescr = kaapi_tracelib_register_fmtdescr(
        1,
        //team->key, [> same key as the team. Not implicit task ? why ? [TG] <]
        (void *)task->id,
        //team->name,
        buff,
        0,
        0          /* no filter: team name should be already well formed */
    );
    int idxtop = koti->pstack.top;
    koti->pstack.stack[idxtop].fdescr = task->fdescr;
    koti->pstack.stack[idxtop].task   = (void*)task->id;

#if LOG
    printf("%i::PUSH task(implicit): thread_id: %i, task: %p, top:%i\n", __LINE__, (int)thread_id, koti->pstack.stack[idxtop].task, idxtop);
#endif

    kaapi_tracelib_task_begin(
        koti->kproc,
        (kaapi_task_id_t)task->id,
        (kaapi_task_id_t)task->pid,
        task->fdescr->fmtid,
        task->isexplicit, /* here implicit task */
        0, 0, 0,
        0, 0,
        koti->pstack.t0,
        0
        );
    ++koti->pstack.top;
    
  }
  else if (endpoint == ompt_scope_end)
  {
    tikki_task_id_t *task = (tikki_task_id_t *)task_data->ptr;
#if LOG
    printf("%" PRIu64 ": ompt_event_implicit_task_action: end. task_id=%" PRIu64 ", @task data=%p\n", thread_id, task->id,task_data);
#endif
    kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
    int idxtop = --koti->pstack.top;
    //kaapi_assert( idxtop == 0 );

#if LOG
    printf("%i::POP task: thread_id: %i, task: %p, top:%i\n", __LINE__, (int)thread_id, koti->pstack.stack[idxtop].task, idxtop);
#endif

    /* reset that accumulated into */
    memset( koti->pstack.stack[idxtop].accum, 0, sizeof(koti->pstack.stack[idxtop].accum));
    kaapi_tracelib_task_end(
        koti->kproc,
        koti->pstack.stack[idxtop].task,
        0, 0,
        koti->pstack.stack[idxtop].fdescr,
        koti->pstack.t0,
        koti->pstack.stack[idxtop].accum
        );
    //free(task);
  } else {
    fprintf(stderr, "%" PRIu64 ": ompt_event_implicit_task_action: unknown endpoint. task_id=%" PRIu64 "\n", thread_id, (uint64_t)task_data->ptr);
  }
}



void
ompt_callback_dependences_action (
    ompt_data_t *task_data,
    const ompt_dependence_t *deps,
    int ndeps
)
{
  tikki_task_id_t *task = (tikki_task_id_t *)task_data->ptr;
  uint64_t thread_id = ompt_get_thread_data()->value;
  task->deps = (ompt_dependence_t *)malloc(ndeps * sizeof(ompt_dependence_t));
#if LOG
  printf("%" PRIu64 ": ompt_event_dependences: tsak_id=%" PRIu64 ", #deps=%i, deps: %p\n", thread_id, task->id, ndeps, task->deps);
#endif
  task->ndeps = ndeps;
  memcpy(task->deps, deps, ndeps*sizeof(ompt_dependence_t));
}



void
ompt_callback_task_dependence_action (
  ompt_data_t *src_task_data,
  ompt_data_t *sink_task_data
)
{
  tikki_task_id_t *src_task = (tikki_task_id_t *)src_task_data->ptr;
  tikki_task_id_t *dest_task = (tikki_task_id_t *)sink_task_data->ptr;
  uint64_t thread_id = ompt_get_thread_data()->value;
  kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];

  kaapi_tracelib_task_depend(
    koti->kproc,
    (kaapi_task_id_t)src_task->id,
    (kaapi_task_id_t)dest_task->id
  );

#if LOG
  printf("%" PRIu64 ": ompt_event_task_dependence: tsak_id=%" PRIu64 ", sink: %" PRIu64 "\n", thread_id, src_task->id, dest_task->id);
#endif
}

void ompt_callback_sync_region_action (
  ompt_sync_region_t kind,
  ompt_scope_endpoint_t endpoint,
  ompt_data_t *parallel_data,
  ompt_data_t *task_data,
  const void *codeptr_ra
)
{
  uint64_t thread_id = ompt_get_thread_data()->value;
  tikki_task_id_t *running_task = (tikki_task_id_t *)task_data->ptr;
#if LOG
  printf("%" PRIu64 ": ompt_callback_sync_region: ", thread_id );
  printf(">, parallel_data:%p, task_data: %p, task_id:%" PRIu64 ",  codeptr_ra: %p\n", parallel_data, task_data, running_task->id, codeptr_ra);
#endif

  switch (endpoint) {
    case ompt_scope_begin:
#if LOG
      printf("BEGIN of kind <");
#endif
      break;

    case ompt_scope_end:
#if LOG
      printf("END of kind <");
#endif
      break;

    case ompt_scope_beginend:
#if LOG
      printf("BEGINEND of kind <");
#endif
      break;
  }

  switch (kind) {
    case ompt_sync_region_taskgroup:
#if LOG
      printf("taskgroup");
#endif
      break;

    case ompt_sync_region_barrier:
#if LOG
      printf("barrier");
#endif
      break;

    case ompt_sync_region_barrier_implicit:
#if LOG
      printf("barrier implicit");
#endif
      break;

    case ompt_sync_region_barrier_explicit:
#if LOG
      printf("barrier explicit");
#endif
      break;

    case ompt_sync_region_barrier_implicit_workshare:
#if LOG
      printf("barrier implicit workshare");
#endif
      break;

    case ompt_sync_region_barrier_implicit_parallel:
#if LOG
      printf("barrier implicit parallel");
#endif
      break;

    case ompt_sync_region_barrier_teams:
#if LOG
      printf("barrier teams");
#endif
      break;

    case ompt_sync_region_barrier_implementation:
#if LOG
      printf("barrier implementation");
#endif
      break;

    case ompt_sync_region_reduction:
#if LOG
      printf("reduction");
#endif
      break;

    case ompt_sync_region_taskwait:
    {
/* Perte du lien running_task -> taskwait -> ... -> taskwait -> running_task.
   car chgt de context (schedule_action
*/
      kaapi_ompt_thread_info_t* koti = &__kaapi_oth_info[thread_id];
      switch (endpoint) {
        case ompt_scope_begin:
          kaapi_tracelib_taskwait_begin(koti->kproc, (kaapi_task_id_t)running_task->id );
          break;

        case ompt_scope_end:
          kaapi_tracelib_taskwait_end(koti->kproc, (kaapi_task_id_t)running_task->id );
          break;
          
        default:
          fprintf(stderr,"*** Error: not handled case endpoint=%i\n",(int)endpoint);
          exit(1);
      }
  
    } break;
  }
}
