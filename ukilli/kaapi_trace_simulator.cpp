/*
** xkaapi
** 
**
** Copyright 2009,2010,2011,2012 INRIA.
**
** Contributors :
**
** Thierry Gautier, thierry.gautier@inrialpes.fr
** 
** This software is a computer program whose purpose is to execute
** multithreaded computation with data flow synchronization between
** threads.
** 
** This software is governed by the CeCILL-C license under French law
** and abiding by the rules of distribution of free software.  You can
** use, modify and/ or redistribute the software under the terms of
** the CeCILL-C license as circulated by CEA, CNRS and INRIA at the
** following URL "http://www.cecill.info".
** 
** As a counterpart to the access to the source code and rights to
** copy, modify and redistribute granted by the license, users are
** provided only with a limited warranty and the software's author,
** the holder of the economic rights, and the successive licensors
** have only limited liability.
** 
** In this respect, the user's attention is drawn to the risks
** associated with loading, using, modifying and/or developing or
** reproducing the software by the user in light of its specific
** status of free software, that may mean that it is complicated to
** manipulate, and that also therefore means that it is reserved for
** developers and experienced professionals having in-depth computer
** knowledge. Users are therefore encouraged to load and test the
** software's suitability as regards their requirements in conditions
** enabling the security of their systems and/or data to be ensured
** and, more generally, to use and operate it in the same conditions
** as regards security.
** 
** The fact that you are presently reading this means that you have
** had knowledge of the CeCILL-C license and that you accept its
** terms.
** 
*/
/* Open a set of event files
*/
#include <stdio.h>
#include <stdint.h>
#define __STDC_FORMAT_MACROS
#include <inttypes.h>
#include "kaapi_trace.h"
#include "kaapi_trace_simulator.h"
#include "kaapi_error.h"
#include <assert.h>
#include <vector>
#include <map>
#include <set>
#include <float.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <strings.h>
#include <stdlib.h>

/** see kaapi_trace.h for coding of event name */
#if (__KAAPI_TRACE_VERSION__ == 10)
#else
#error "Unknown trace version in this source file"
#endif

/* This stack size correspond to the initial maximal deepth of tasks supported
   by the simulation
*/
#define TASK_STACKSIZE 1024

struct Simulator;

typedef int kaapi_processor_id_t;
typedef int kaapi_format_id_t;
typedef struct kaapi_task kaapi_task_t;

struct stacktask_entry_t {
  uint64_t          date;
  kaapi_task_t*     task;
  kaapi_format_id_t fmtid;
};

struct ProcessorState {
  Simulator* sim;
  kaapi_processor_id_t kid;   /* processor identifier */
  int      active;            /* 1 iff active, 0 iff idle */
  uint64_t taskcount;
  uint64_t stealcount;        /* successfull steal operation */
  uint64_t tactive;           /* time where is active  */
  uint64_t tidle;             /* time where is idle */
  uint64_t tcompute;          /* time where is passed in task */
  uint64_t lastdate;          /* last date when activity state changed */
  uint64_t lasttactive;       /* time where is active  */
  uint64_t lasttidle;         /* time where is idle */
  uint64_t firstdate;         /* start time */
  uint64_t finaldate;         /* final time */
  uint64_t tinf;              /* critical path unit== nanosecond */
  uint64_t tmax;              /* statistics about task execution unit== second*/  
  uint64_t tmin;
  uint64_t tmain;
  double   tsum;              /* sum of execution time */
  double   t2;                /* sum t*t */
  int      sp;                /* stack pointer */
  stacktask_entry_t* stacktask;        /* stack of start time for each stack: unit= nano second */
  int      stackdeepth;       /* maximal number of deepth in the stack */
  kaapi_perf_counter_t	   perfctr[KAAPI_PERF_ID_MAX];
  kaapi_perf_counter_t	   lasttask_perfctr[KAAPI_PERF_ID_MAX]; /**/
  kaapi_perf_idset_t  	   lasttask_perfctr_mask;
};

/* state of the simulator */
struct Simulator {
  FileSet*                    fds;
  std::map<int,ProcessorState*> procs;
  std::vector<int>            kids;
  std::set<uint64_t>          fmtid_filter;
  int                         discretetime;  /* !=0 if discrete time=>report events at their date */
  int                         flag_taskdump;
  int                         fd_taskdump;
  int                         (*task_filter)( struct Simulator*, kaapi_format_id_t );
  uint64_t                    hres;  /* in ns */
  uint64_t                    tmin;  /* in s */
  uint64_t                    tmax;  /* in s */
  uint64_t                    tsim;  /* in ns, start from 0 (offset with epoch == tmin) */
  double*                     efficiencies;
  uint64_t*                   count_stealok;
  uint64_t*                   count_task;
};


static void processor_simulate_event(
  Simulator* sim,
  ProcessorState* p,
  const kaapi_event_t* event
);


static int default_task_filter( Simulator* sim, kaapi_format_id_t )
{
  return 1;
}

static int task_filter_value( Simulator* sim, kaapi_format_id_t fmtid)
{
  return sim->fmtid_filter.find( fmtid ) != sim->fmtid_filter.end() ? 1 : 0;
}

/* Return the efficiency of the processor at the given date 
   tsim is the next sampling time.
*/
static double ProcessorEfficiencyAt( ProcessorState* proc, uint64_t tsim )
{
  uint64_t tidle;
  uint64_t tactive;
  uint64_t diff_tidle;
  uint64_t diff_tactive;
  
  if (proc->lastdate ==0) return 0;

  assert(proc->lastdate <= tsim);
  
  tidle   = proc->tidle;
  tactive = proc->tactive;

  /* update with state between lastdate and tsim */
  if (proc->active ==1) 
    tactive += (tsim - proc->lastdate);
  else
    tidle   += (tsim - proc->lastdate);

  /* monotone increasing functions bound by tsim ! */
  assert( tactive >= proc->tactive);
  assert( tidle >= proc->tidle);
  assert( tactive <= tsim );
  assert( tidle <= tsim );

  /* diff with previous sampling time at t0 */
  assert( tidle >= proc->lasttidle);
  diff_tidle  = tidle - proc->lasttidle;
  assert( tactive >= proc->lasttactive);
  diff_tactive = tactive - proc->lasttactive;
  
  /* */
  proc->lasttactive = tactive;
  proc->lasttidle   = tidle;
  
  if ((diff_tactive ==0) || (diff_tidle ==0))
    return 0;
  return (double)(diff_tactive) / (double)(diff_tactive+diff_tidle);
}


/*
*/
extern "C"
Simulator* OpenSimulator( FileSet* fds,
    double hres,
    int discrete,
    int flag_taskdump,
    unsigned int  fmtid_filter_count,
    uint64_t* fmtid_filter_values
 )
{
  int i, err;
  int count;
  
  Simulator* sim = new Simulator;
  if (sim ==0) return 0;
  sim->fds   = fds;
  sim->hres  = (uint64_t)(hres * 1e9); /* count in ns */
  sim->tsim  = 0;

  err = GetInterval(fds, &sim->tmin, &sim->tmax);
  if (err !=0)
    goto return_on_error;

  count = GetProcessorCount(fds);
  if (count <=0) 
    goto return_on_error;

  sim->procs.clear();
  sim->kids.reserve( count );
  sim->efficiencies  = (double*)malloc(sizeof(double) * count );
  sim->count_stealok = (uint64_t*)malloc(sizeof(uint64_t) * count );
  sim->count_task    = (uint64_t*)malloc(sizeof(uint64_t) * count );
  sim->discretetime  = (discrete !=0);
  sim->flag_taskdump = flag_taskdump;
  if (flag_taskdump)
  {
    /* Error using O_CREAT with no third parameter on Ubuntu */
    sim->fd_taskdump = open("task.time", O_WRONLY|O_CREAT|O_TRUNC, S_IRUSR | S_IWUSR);
    if (sim->fd_taskdump == -1)
    {
      fprintf(stderr, "*** Cannot create 'task.time', error: '%s'\n", strerror(errno));
      fflush(stderr);
    }
    else {
      fprintf(stdout, "*** File 'task.time' created\n");
      fflush(stdout);

      kaapi_eventfile_header_t header;
      if (GetHeader(fds, &header) ==0)
      {
        int size = 1024;
        char* comment = (char*)malloc(size);
        char* buffer = comment;
        /* write format for the file */
        char unitdate[15];
        snprintf(unitdate, 15, "date(%s)",header.event_date_unit);
        int c = snprintf(buffer, size, "%5s %12s %12s %8s", "kid", unitdate, "duration", "task_id");
        buffer += c;

        int i, count_perfcounter = header.perfcounter_count & 0xFF;
        for (i=0; i<count_perfcounter; ++i)
        {
           if (kaapi_perf_idset_test(&header.task_perf_mask, i))
           {
             c = snprintf(buffer, comment+size-buffer, " %16s", header.perfcounter_name[i]);
             buffer += c;
             kaapi_assert( comment - buffer < size );
           }
        }
        c = snprintf(buffer, comment+size-buffer, "\n");
        buffer += c;
        kaapi_assert( comment - buffer < size );
        write(sim->fd_taskdump, comment, strlen(comment));
        free(comment);
      }
    }
    if ((fmtid_filter_count >0) && (fmtid_filter_values !=0))
    {
      sim->fmtid_filter.clear();
      for (unsigned int i=0; i<fmtid_filter_count; ++i)
        sim->fmtid_filter.insert( fmtid_filter_values[i] );
      sim->task_filter = task_filter_value;
    }
    else
      sim->task_filter = default_task_filter;

  }
  else 
  {
    sim->fd_taskdump = -1;
  }

  for (i=0;i<count; ++i)
  {
    ProcessorState* p = new ProcessorState;
    p->kid        = GetProcessorId( fds, i );
    p->sim        = sim;
    p->active     = 0;
    p->taskcount  = 0;
    p->stealcount = 0;
    p->tactive    = 0;
    p->tidle      = 0;
    p->lastdate   = 0;
    p->lasttactive= 0;
    p->lasttidle  = 0;
    p->firstdate  = 0;
    p->finaldate  = 0;
    p->tmax       = 0;
    p->tmin       = ~0UL;
    p->tmain      = 0;
    p->tsum       = 0;
    p->t2         = 0;
    p->sp         = -1; /* no task first ! */
    p->stackdeepth= TASK_STACKSIZE;
    p->stacktask  = (stacktask_entry_t*)calloc(sizeof(stacktask_entry_t), p->stackdeepth);
    if (p->stacktask ==0)
      kaapi_abort(__LINE__, __FILE__, "Error: cannot allocate memory");

    bzero(&p->perfctr, sizeof(p->perfctr));
    kaapi_perf_idset_zero(&p->lasttask_perfctr_mask);

    sim->procs.insert( std::make_pair(p->kid, p) );
    sim->kids.push_back( p->kid );
  }
  
  return sim;

return_on_error:
  delete sim;
  return 0;
}



extern "C"
double NextStepSimulator( Simulator* sim )
{
  const kaapi_event_t* event;
  uint64_t nextTsim =0; /* used for continuous mode */

  /* simulate until global date sim->tmin + sim->tsim + sim->hres */
  if (sim->discretetime ==0)
    nextTsim   = sim->tmin+sim->tsim+sim->hres;
    
  if (EmptyEvent(sim->fds))
  {
    return -1.0;
  }
  
  while (!EmptyEvent(sim->fds))
  {
    event = TopEvent(sim->fds);
    uint32_t kid = event->kid;

    if (sim->discretetime ==0)
    {
      if (event->date >= nextTsim)
        break;
      
      processor_simulate_event( sim, sim->procs[kid], event );
      
      NextEvent(sim->fds);
    }
    else
    {
      ProcessorState* proc = sim->procs[kid];
      processor_simulate_event( sim, proc, event );
      NextEvent(sim->fds);
      sim->tsim = event->date - sim->tmin;
      break;
    }
  }
  if (sim->discretetime ==0)
    sim->tsim += sim->hres;
  return nanosecond2second(sim->tsim);
}


/* do accumulation on data
*/
static int AccumulOneProcDataSimulator( struct Simulator* sim, int kid,
    double*      data
)
{
  ProcessorState* proc = sim->procs[kid];
  ///* update global information. Do not pop event */
  //data[KASIM_DATA_DATE]       += nanosecond2second(sim->tsim);
  /* update global information. Do not pop event */
  double d = nanosecond2second_diff(proc->finaldate, proc->firstdate);
  if (data[KASIM_DATA_DURATION] < d)
    data[KASIM_DATA_DURATION] =  d;
  
  data[KASIM_DATA_TASK]       += (double)proc->taskcount;
  data[KASIM_DATA_COMPUTE]    += nanosecond2second(proc->tcompute);
  data[KASIM_DATA_IDLE]       += nanosecond2second(proc->tidle);
  data[KASIM_DATA_TASKDEEP]   += (double)proc->sp+1;

  sim->count_stealok[kid]     += (double)proc->stealcount;
  sim->efficiencies[kid]      += ProcessorEfficiencyAt( proc, sim->tsim+sim->tmin );

  data[KASIM_DATA_STEALOP]    += sim->count_stealok[kid];
  data[KASIM_DATA_EFFICIENCY] += sim->efficiencies[kid];

  double tinf = nanosecond2second(proc->tinf);
  if (data[KASIM_DATA_TINF] < tinf)
    data[KASIM_DATA_TINF] = tinf;

  for (int i=KASIM_DATA_PERFCTR; i<KASIM_MAX_COUNTER; ++i)
    data[i] += proc->perfctr[i-KASIM_DATA_PERFCTR];
  return 0;
}

/* do accumulation on data
*/
static int AccumulOneStatDataSimulator( struct Simulator* sim, int kid,
    double*      data
)
{
  ProcessorState* proc = sim->procs[kid];

  /* update global information. Do not pop event */
  double d = nanosecond2second_diff(proc->finaldate, proc->firstdate);
  if (data[KASIM_DATA_DURATION] < d)
    data[KASIM_DATA_DURATION] =  d;

  data[KASIM_DATA_TASK]    += (double)proc->taskcount;
  data[KASIM_DATA_COMPUTE] += nanosecond2second(proc->tcompute);
  data[KASIM_DATA_IDLE]    += nanosecond2second(proc->tidle);
  data[KASIM_DATA_STEALOP] += (double)proc->stealcount;

  double tinf = nanosecond2second(proc->tinf);
  if (data[KASIM_DATA_TINF] < tinf)
    data[KASIM_DATA_TINF] = tinf;
  double tmin = nanosecond2second(proc->tmin);
  double tmax = nanosecond2second(proc->tmax);
  if (tmin < data[KASIM_DATA_TASKMIN])
    data[KASIM_DATA_TASKMIN] = tmin;
  if (tmax > data[KASIM_DATA_TASKMAX])
    data[KASIM_DATA_TASKMAX] = tmax;
  data[KASIM_DATA_TASKAVRG]  += nanosecond2second(proc->tcompute);
  return 0;
}

/*
*/
extern "C"
int OneProcDataSimulator( struct Simulator* sim, int ith,
    double*      data
)
{
  for (int i=0; i<KASIM_MAX_COUNTER; ++i)
    data[i] = 0;
  return AccumulOneProcDataSimulator( sim, sim->kids[ith], data );
}


/*
*/
extern "C"
int ProcDataSimulator( struct Simulator* sim,
    double*      data
)
{
  for (int i=0; i<KASIM_MAX_COUNTER; ++i)
    data[i] = 0;
  
  int count = GetProcessorCount(sim->fds);
  for (int i= 0; i<count; ++i)
  {
    AccumulOneProcDataSimulator( sim, sim->kids[i], data );
  }
  
  /* for date, stack deep and efficiency, compute average */
  data[KASIM_DATA_DATE] /= (double)count;
  data[KASIM_DATA_TASKDEEP] /= (double)count;
  data[KASIM_DATA_EFFICIENCY] /= (double)count;
  return 0;
}

/**/
extern "C"
int OneStatDataSimulator( struct Simulator* sim, int ith,
    double*      data
)
{
  for (int i=0; i<KASIM_MAX_STATCOUNTER; ++i)
    data[i] = 0;
  data[KASIM_DATA_TASKMIN] = DBL_MAX;
  AccumulOneStatDataSimulator( sim, sim->kids[ith], data );
  data[KASIM_DATA_TASKAVRG]  /= data[KASIM_DATA_TASK];
  return 0;
}

/**/
extern "C"
int StatDataSimulator( struct Simulator* sim,
    double*      data
)
{
  for (int i=0; i<KASIM_MAX_STATCOUNTER; ++i)
    data[i] = 0;
  data[KASIM_DATA_TASKMIN] = DBL_MAX;
  
  std::map<int,ProcessorState*>::iterator beg = sim->procs.begin();
  std::map<int,ProcessorState*>::iterator end = sim->procs.end();
  int nbproc = 0;
  while (beg !=end)
  {
    AccumulOneStatDataSimulator( sim, beg->second->kid, data );
    ++beg;
    ++nbproc;
  }
  
  /* for date, task time, compute average */
  //data[KASIM_DATA_DATE] /= (double)nbproc;
  data[KASIM_DATA_TASKAVRG]  /= data[KASIM_DATA_TASK];
  return 0;
}


/* Do one step of simulation and return the synthetized information
   (date, count, efficiencies[i]) for each processor.
   On return the next simulation date is date+hres (contiguous simulation mode)
   or date of the next event.
*/
extern "C"
int OneStepSimulator( Simulator*       sim,
                      double*          date,
                      int*             count, 
                      const double**   ef,
                      const uint64_t** cs
)
{
  *date = NextStepSimulator(sim);
  if (*date >=0.0)
  {
    double data[KASIM_MAX_COUNTER];
    for (int i= 0; i<KASIM_MAX_COUNTER; ++i)
      data[i] = 0.0;

    std::map<int,ProcessorState*>::iterator beg = sim->procs.begin();
    std::map<int,ProcessorState*>::iterator end = sim->procs.end();
    int nbproc = 0;
    while (beg !=end)
    {
      AccumulOneProcDataSimulator( sim, beg->second->kid, data );
      ++beg;
      ++nbproc;
    }
    
    *count = nbproc;
    *ef = sim->efficiencies;
    *cs = sim->count_stealok;
    return  1;
  }
  return 0;
}


/* Read and call callback on each event, ordered by date
*/
extern "C"
int CloseSimulator(Simulator* sim )
{
  if (sim ==0) return 0;
  free(sim->efficiencies);
  free(sim->count_stealok);
  free(sim->count_task);
  
  std::map<int,ProcessorState*>::iterator beg = sim->procs.begin();
  std::map<int,ProcessorState*>::iterator end = sim->procs.end();
  while (beg !=end)
  {
    ProcessorState* p = beg->second;
    free( p->stacktask);
    delete p;
    ++beg;
  }
  sim->procs.clear();

  if (sim->fd_taskdump != -1)
    close(sim->fd_taskdump);
  delete sim;
  return 0;
}


/*
*/
static void processor_simulate_event(
  Simulator* sim,
  ProcessorState* proc,
  const kaapi_event_t* event
)
{
//printf("%lu  evt:%i -> %s\n", event->date, event->evtno, kaapi_event_name[event->evtno]);
  switch (event->evtno) 
  {
    case KAAPI_EVT_KPROC_START:
      ++proc->active;
      proc->firstdate = event->date;
      proc->lastdate  = event->date;
      proc->tidle     = 0;
      proc->tactive   = 0;
      break;

    case KAAPI_EVT_KPROC_STOP:
      assert( event->date >= proc->lastdate);
      proc->tactive += (event->date - proc->lastdate);
      assert( proc->tactive <= event->date);
      proc->lastdate  = event->date;
      proc->finaldate = event->date;
      --proc->active;
      break;


    case KAAPI_EVT_TASK_BEG:
    {
      ++proc->taskcount;
      proc->sp++;
      if (proc->sp >= proc->stackdeepth)
      {
        int newsize = proc->stackdeepth *2;
        proc->stacktask = (stacktask_entry_t*)realloc(proc->stacktask, newsize*sizeof(stacktask_entry_t));
        if (proc->stacktask ==0)
          kaapi_abort(__LINE__, __FILE__, "Error: cannot allocate memory");
      }
      proc->stacktask[proc->sp].date = event->date;
      proc->stacktask[proc->sp].task = (kaapi_task_t*)KAAPI_EVENT_DATA(event,0,p);
      proc->stacktask[proc->sp].fmtid = (kaapi_format_id_t)KAAPI_EVENT_DATA(event,1,i);
      //proc->stacktask[proc->sp].kid  = (kaapi_processor_id_t)KAAPI_EVENT_DATA(event,2,i);
    } break;

    case KAAPI_EVT_TASK_ACCESS:
    break;

    case KAAPI_EVT_TASK_DATA:
    break;

    case KAAPI_EVT_TASK_SUCC:
    break;

    case KAAPI_EVT_TASK_STEAL:
    break;

    case KAAPI_EVT_TASK_PERFCOUNTER:
    {
      for (int i=0; ; ++i)
      {
        if (KAAPI_EVENT_DATA(event,1,i8)[i] == (uint8_t)-1) break;
        proc->lasttask_perfctr[KAAPI_EVENT_DATA(event,1,i8)[i]] = event->u.data[i+2].u;
        kaapi_perf_idset_add(&proc->lasttask_perfctr_mask, KAAPI_EVENT_DATA(event,1,i8)[i]);
      }
    } break;

    case KAAPI_EVT_TASK_END:
    {
      if (proc->sp <0) 
      {
        fprintf(stderr, "*** Invalid event End of task\n");
        fflush(stderr);
        proc->sp = -1;
        break;
      }
      if (proc->stacktask[proc->sp].task != (kaapi_task_t*)KAAPI_EVENT_DATA(event,0,p))
        kaapi_abort(__LINE__, __FILE__, "Error: missmatch between task begin/task end events");

      uint64_t delay = event->date - proc->stacktask[proc->sp].date;
      if (delay < proc->tmin) proc->tmin = delay;
      if (delay > proc->tmax) proc->tmax = delay;
      if ((sim->fd_taskdump !=-1) && (sim->task_filter( sim, proc->stacktask[proc->sp].fmtid )))
      {
        static char tmp[2048];
        char* buffer = tmp;
        int c = snprintf(buffer, 2048, "%5lu %12llu %12lld %8u",
            (unsigned long)event->kid,                            /* kid */
            (unsigned long long) proc->stacktask[proc->sp].date,  /* start date */
            (long long) (event->date - proc->stacktask[proc->sp].date),  /* duration */
            (unsigned)proc->stacktask[proc->sp].fmtid             /* type */
        );
        kaapi_assert(event->date > proc->stacktask[proc->sp].date);
        buffer += c;
//printf("%llu: #evts: %i\n", event->date, __builtin_popcountll( proc->lasttask_perfctr_mask ) );
        if (!kaapi_perf_idset_empty(&proc->lasttask_perfctr_mask))
        {
          while (proc->lasttask_perfctr_mask !=0)
          {
            int idx = __builtin_ffsl( proc->lasttask_perfctr_mask) -1;
            kaapi_perf_idset_clear(&proc->lasttask_perfctr_mask, idx );
//printf("PerfIdx: %i ->%s\n", idx, kaapi_perfctr_info[idx].name );
            if (kaapi_perfctr_info[idx].ns2s)
              c = snprintf(buffer, tmp+2048-buffer, " %16e", 1e-9*(double)proc->lasttask_perfctr[idx] );
            else
              c = snprintf(buffer, tmp+2048-buffer, " %16" PRIi64, proc->lasttask_perfctr[idx]);
            buffer += c;
            kaapi_assert( buffer <= tmp + 256);
          }
        }
        c = snprintf(buffer, tmp+2048-buffer, "\n");
        write(proc->sim->fd_taskdump,tmp, strlen(tmp));
      }
      kaapi_perf_idset_zero(&proc->lasttask_perfctr_mask);

      proc->tsum += delay;
      proc->tcompute += delay;
      if (proc->tinf < KAAPI_EVENT_DATA(event,2,i))
        proc->tinf = KAAPI_EVENT_DATA(event,2,i);
      --proc->sp;
    } break;

    /* idle = steal state */
    case KAAPI_EVT_SCHED_SUSPWAIT_BEG:
    case KAAPI_EVT_SCHED_IDLE_BEG:
      assert( proc->active >= 1);
      assert( event->date >= proc->lastdate);
      proc->tactive += (event->date - proc->lastdate);
      assert( proc->tactive <= event->date);
      proc->lastdate = event->date;
      --proc->active;
    break;

    case KAAPI_EVT_SCHED_SUSPWAIT_END:
    case KAAPI_EVT_SCHED_IDLE_END:
      assert( proc->active >= 0);
      assert( event->date >= proc->lastdate);
      proc->tidle   += (event->date - proc->lastdate);
      assert( proc->tidle <= event->date);
      proc->lastdate = event->date;
      ++proc->active;
    break;

    /* processing request */
    case KAAPI_EVT_REQUEST_BEG:
    break;

    case KAAPI_EVT_REQUEST_END:
    break;

    /* emit steal */
    case KAAPI_EVT_STEAL_OP:
    break;

    case KAAPI_EVT_PERFCOUNTER:
      proc->perfctr[KAAPI_EVENT_DATA(event,0,i)] = event->u.data[1].u;
    break;

    case KAAPI_EVT_PARALLEL :
    case KAAPI_EVT_TASKWAIT :
    case KAAPI_EVT_TASKGROUP:
    break;

    case KAAPI_EVT_BARRIER:
    case KAAPI_EVT_LOOP_BEGIN:
    case KAAPI_EVT_LOOP_END  :
    case KAAPI_EVT_LOOP_NEXT :
    break;

    default:
      printf("***Unknown event number: %i\n", event->evtno);
      break;
  }
}
